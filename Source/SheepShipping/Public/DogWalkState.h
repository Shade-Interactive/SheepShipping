// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "State.h"
#include "DogWalkState.generated.h"

class ASDogCharacter;
class APlayerDogController;

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class SHEEPSHIPPING_API UDogWalkState : public UState
{
	GENERATED_BODY()

	
public:
	virtual TSubclassOf<UState> UpdateStateSubClass() override;


	virtual void BeginState() override;

	virtual void EndState() override;

	virtual void InitializeState() override;

protected:
	UPROPERTY(VisibleAnywhere, Category = "StateMachine")
		ASDogCharacter* DogCharacter;


	UPROPERTY(EditDefaultsOnly, Category = "StateMachine")
		TSubclassOf<UState> StateIdleClassType;


};
