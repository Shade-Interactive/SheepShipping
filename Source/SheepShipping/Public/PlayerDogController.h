// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "PlayerDogController.generated.h"

class UState;



/**
 * 
 */
UCLASS()
class SHEEPSHIPPING_API APlayerDogController : public APlayerController
{
	GENERATED_BODY()
	
public:

	virtual void PlayerTick(float DeltaTime) override;

	
	
protected:
	virtual void SetupInputComponent() override;
	void JumpInputPressed();
	
	void InputForward(float value);
	void InputRight(float value);

};
