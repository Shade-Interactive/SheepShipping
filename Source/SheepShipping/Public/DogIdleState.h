// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "State.h"
#include "DogIdleState.generated.h"

class Ustate;
class ASDogCharacter;
class APlayerDogController;

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class SHEEPSHIPPING_API UDogIdleState : public UState
{
	GENERATED_BODY()

public:
	//Implement end state from state class
	virtual void EndState() override;
	//Implement begin state from state class
	virtual void BeginState() override;
	//Implement update state from state class
	virtual TSubclassOf<UState> UpdateStateSubClass() override;

	virtual void InitializeState() override;
	
	UFUNCTION()
	void IdleToJump();

protected:

	UPROPERTY(VisibleAnywhere, Category="StateMachine")
		ASDogCharacter* DogCharacter;
	
	///TODOD: Remove the dog player controller from states!!!! The system can't work with AI controller. It is the player controller in charge of sending information to pawn and states of state machine get information from to the player controller.


	UPROPERTY(EditDefaultsOnly, Category = "StateMachine")
		TSubclassOf<UState> StateWalkClassType;
	UPROPERTY(EditDefaultsOnly, Category = "StateMachine")
		TSubclassOf<UState> StateJumpClassType;

private:
	bool ActivateJump = false;
};
