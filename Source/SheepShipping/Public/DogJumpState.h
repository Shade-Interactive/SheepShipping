// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "State.h"
#include "DogJumpState.generated.h"

class ASDogCharacter;
class UState;

/**
 * 
 */
UCLASS()
class SHEEPSHIPPING_API UDogJumpState : public UState
{
	GENERATED_BODY()
	
public:
	virtual TSubclassOf<UState> UpdateStateSubClass() override;

	virtual void BeginState() override;

	virtual void EndState() override;
protected:

	UPROPERTY(EditAnywhere, Category = "StateMachine")
		ASDogCharacter* DogCharacter;
	UPROPERTY(EditDefaultsOnly, Category = "StateMachine")
		TSubclassOf<UState> IdleState;

};
