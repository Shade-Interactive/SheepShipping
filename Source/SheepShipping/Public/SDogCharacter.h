// Fill out yor copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "SDogCharacter.generated.h"

class UState;
class UDogIdleState;
class UStateMachine;
class UDogWalkState;
class UDogJumpState;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FActionSignature);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FControllerSignature, float, inputValue);


UCLASS()
class SHEEPSHIPPING_API ASDogCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASDogCharacter();
	
	/*Return the speed of the player (for animation)*/
	UFUNCTION(BlueprintPure, Category="StateMachine")
	float GetSpeed();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite, Category = "StateMachine")
	UStateMachine* StateMachine;
	UPROPERTY(EditDefaultsOnly, Category = "StateMachine")
	UDogIdleState * IdleState;
	UPROPERTY(EditDefaultsOnly, Category = "StateMachine")
	UDogWalkState* WalkState;
	UPROPERTY(EditDefaultsOnly, Category = "StateMachine")
	UDogJumpState* JumpState;
	
	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void Move(float ForwardValue, float RightValue);
	
	void ActionJumpReceived();
	void InputForwardReceived(float ForwardValue);
	void InputRightReceived(float RightValue);

	float GetForwardValue();
	float GetRightValue();

	FActionSignature OnActionJumpReceived;
	FControllerSignature OnInputForwardReceived;
	FControllerSignature OnInputRightReceived;

	float ForwardInput = 0;
	float RightInput = 0;

private:
	bool isAllowedToJump;

	float Speed = 0;
	float Direction;

	
};
