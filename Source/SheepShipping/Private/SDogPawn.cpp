// Fill out your copyright notice in the Description page of Project Settings.

#include "SDogPawn.h"


// Sets default values
ASDogPawn::ASDogPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASDogPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASDogPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ASDogPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

