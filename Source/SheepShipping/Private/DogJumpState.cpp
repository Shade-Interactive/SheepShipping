// Fill out your copyright notice in the Description page of Project Settings.

#include "DogJumpState.h"
#include "SDogCharacter.h"




TSubclassOf<UState> UDogJumpState::UpdateStateSubClass()
{
	Super::UpdateStateSubClass();

	if (DogCharacter->bWasJumping != true)
	{
		return IdleState;
	}
	
	return nullptr;
}

void UDogJumpState::BeginState()
{
	Super::BeginState();

	DogCharacter = Cast<ASDogCharacter>(GetOwner());
	DogCharacter->Jump();
	
}

void UDogJumpState::EndState()
{
	Super::EndState();
}
