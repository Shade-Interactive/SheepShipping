// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerDogController.h"
#include "SDogCharacter.h"
#include "State.h"

void APlayerDogController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);


}



void APlayerDogController::SetupInputComponent()
{
	//ASDogCharacter* dog = Cast<ASDogCharacter>(GetPawn());
	Super::SetupInputComponent();

	InputComponent->BindAction("Jump",IE_Pressed, this, &APlayerDogController::JumpInputPressed);
	InputComponent->BindAxis("Forward", this, &APlayerDogController::InputForward);
	InputComponent->BindAxis("Right", this, &APlayerDogController::InputRight);
}

void APlayerDogController::JumpInputPressed()
{
	//OnJumpClicked.Broadcast();
	
	ASDogCharacter* DogCharacter = Cast<ASDogCharacter>(GetCharacter());
	DogCharacter->ActionJumpReceived();
}

void APlayerDogController::InputForward(float value)
{
	//UE_LOG(LogTemp, Warning, TEXT("input value forward = %s"), *FString::SanitizeFloat(value));
	ASDogCharacter* DogCharacter = Cast<ASDogCharacter>(GetCharacter());
	DogCharacter->InputForwardReceived(value);

}

void APlayerDogController::InputRight(float value)
{

	ASDogCharacter* DogCharacter = Cast<ASDogCharacter>(GetCharacter());
	DogCharacter->InputRightReceived(value);

}

