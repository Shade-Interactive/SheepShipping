// Fill out your copyright notice in the Description page of Project Settings.

#include "SDogCharacter.h"
#include "StateMachine.h"
#include "DogIdleState.h"
#include "DogWalkState.h"
#include "DogJumpState.h"



// Sets default values
ASDogCharacter::ASDogCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	StateMachine = CreateDefaultSubobject<UStateMachine>(TEXT("StateMachineComp"));
	IdleState = CreateDefaultSubobject<UDogIdleState>(TEXT("IdleState"));
	WalkState = CreateDefaultSubobject<UDogWalkState>(TEXT("WalkState"));
	JumpState = CreateDefaultSubobject<UDogJumpState>(TEXT("JumpState"));

}

float ASDogCharacter::GetSpeed()
{
	if (this != nullptr)
	{
		return Speed;
	}
	
	return 0;
}

// Called when the game starts or when spawned
void ASDogCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	Speed = 0;


}

// Called every frame
void ASDogCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASDogCharacter::Move(float ForwardValue, float RightValue)
{
	this->AddMovementInput(this->GetActorForwardVector(), ForwardValue);
	this->AddMovementInput(this->GetActorRightVector(), RightValue);

	//Assign speed
	Speed = this->GetVelocity().Size();
	
	//Assign dog mesh direction
	float DirectionRad = FMath::Atan2(RightValue,ForwardValue);
	Direction = FMath::RadiansToDegrees(DirectionRad);

	//TODO: Add an offest on the mesh direction accessible by blueprint
	FRotator myrotator = FRotator(0.f, Direction - 90.f,0.f );
	GetMesh()->SetRelativeRotation(myrotator);
	
}

void ASDogCharacter::ActionJumpReceived()
{
	OnActionJumpReceived.Broadcast();
}

void ASDogCharacter::InputForwardReceived(float ForwardValue)
{

	ForwardInput = ForwardValue;
	OnInputForwardReceived.Broadcast(ForwardValue);
	
}

void ASDogCharacter::InputRightReceived(float RightValue)
{

	RightInput = RightValue;
	OnInputRightReceived.Broadcast(RightValue);

}

float ASDogCharacter::GetForwardValue()
{
	return ForwardInput;
}

float ASDogCharacter::GetRightValue()
{
	return RightInput;
}