// Fill out your copyright notice in the Description page of Project Settings.

#include "DogIdleState.h"
#include "SDogCharacter.h"
#include "Components/InputComponent.h"
#include "PlayerDogController.h"


void UDogIdleState::BeginState()
{
	Super::BeginState();
	//Reset the jump value
	ActivateJump = false;

	if (DogCharacter)
	{
		DogCharacter->OnActionJumpReceived.AddDynamic(this, &UDogIdleState::IdleToJump);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("No player dog controller detected in %s"), *this->GetName());
	}
}

void UDogIdleState::InitializeState()
{
	Super::InitializeState();

	DogCharacter = Cast<ASDogCharacter>(GetOwner());
	

}

TSubclassOf<UState> UDogIdleState::UpdateStateSubClass()
{

	if (ActivateJump)
	{
		return StateJumpClassType;
	}

	if (DogCharacter->GetForwardValue() != 0 || DogCharacter->GetRightValue() != 0)
	{
		return StateWalkClassType;
	}
		
	return nullptr;
}

void UDogIdleState::EndState()
{
	Super::EndState();

	DogCharacter->OnActionJumpReceived.RemoveDynamic(this, &UDogIdleState::IdleToJump);
	
}


void UDogIdleState::IdleToJump()
{
	ActivateJump = true;
}
