// Fill out your copyright notice in the Description page of Project Settings.

#include "DogWalkState.h"
#include "SDogCharacter.h"
#include "PlayerDogController.h"



TSubclassOf<UState> UDogWalkState::UpdateStateSubClass()
{

	if (DogCharacter->GetForwardValue() == 0 && DogCharacter->GetRightValue() == 0)
	{
		return StateIdleClassType;
	}

	DogCharacter->Move(DogCharacter->GetForwardValue(), DogCharacter->GetRightValue());

	return nullptr;
}

void UDogWalkState::BeginState()
{
	Super::BeginState();
}

void UDogWalkState::EndState()
{
	Super::EndState();
}

void UDogWalkState::InitializeState()
{
	Super::InitializeState();

	DogCharacter = Cast<ASDogCharacter>(GetOwner());
	
}
