// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "State.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FInternalStateSignature);

UCLASS( ClassGroup=(Custom) , meta=(BlueprintSpawnableComponent, ChildCannotTick) )
class FINITESTATEMACHINE_API UState : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UState();

	UPROPERTY(EditAnywhere,Category="StateMachine")
		bool DisplayLogs = false;

	FInternalStateSignature OnStateBegan;
	FInternalStateSignature OnStateEnded;

protected:

public:
	/**
	 * Overitable initialize state. 
	 This phase happens during the begin play of the state machine and happens only one time.
	 */
	UFUNCTION(BlueprintCallable, Category = "StateMachine")
		virtual void InitializeState();

	/**
	 * Overitable update state state (Managed by state machine)
	 
	 @ Return the subclass type of the new state. Return null if no state is needed during this frame.
	 */
	UFUNCTION(BlueprintCallable, Category = "StateMachine")
	virtual TSubclassOf<UState> UpdateStateSubClass();

	/**
	* Overitable begin state (Managed by state machine)
	*/
	UFUNCTION(BlueprintCallable, Category = "StateMachine")
	virtual void BeginState();

	/**
	* Overitable end state (Managed by state machine)
	*/
	UFUNCTION(BlueprintCallable, Category = "StateMachine")
	virtual void EndState();
private:
	
};
