// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "StateMachine.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FStateMachineSignature, class UState*, ReturnedState);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FStateMachineSignatureTwo, class UState*, NewState, class UState*, LastState);

class UState;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FINITESTATEMACHINE_API UStateMachine : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UStateMachine();
	// called at the begining of the game
	virtual void BeginPlay();
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
#if WITH_EDITOR
	// Called when property change in the blueprint
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif
	UPROPERTY(BlueprintAssignable)
	FStateMachineSignature OnLastStateEnded;
	UPROPERTY(BlueprintAssignable)
	FStateMachineSignature OnNewStateBegan;
	UPROPERTY(BlueprintAssignable)
	FStateMachineSignatureTwo OnNewStateBeganTwoParam;

	UFUNCTION(BlueprintCallable, Category = "StateMachine")
		void SetLogDisplayed(bool DisplayLogs);
	UFUNCTION(BlueprintPure, Category = "StateMachine")
		bool GetIsLogDisplayed();


protected:

	//Entry state type (defined by the user in editor)
	UPROPERTY(EditDefaultsOnly, Category="StateMachine")
	TSubclassOf<UState> ClassEntryState;
	//All states of the state machine (Assigned during the begin play)
	UPROPERTY(VisibleAnywhere,Category = "StateMachine")
	TArray<UState*> AllStates;
	///TODO: Manage all states in the constructor to create a nodal view of the system.

	UState* GetStateComponentFromStateClassType(TSubclassOf<UState> StateType);
	
private:

	//Entry state based on the class entry state
	UPROPERTY()
	UState* EntryState;
	//Current state of the state machine.
	UPROPERTY()
	UState* CurrentState;
	//Last state of state machine
	UPROPERTY()
	UState* LastState;


	bool LogsDisplayed = false;

	/**
	 * Initilize all state of the current state machine.
	 */
	void InitializeAllStates();


	/**
	* Set entry state from code.
	* @state selected as entry state
	*/
	void SetEntryState(UState* NewCurrentState);

};
