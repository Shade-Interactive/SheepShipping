// Fill out your copyright notice in the Description page of Project Settings.

#include "State.h"



// Sets default values for this component's properties
UState::UState()
{
	
}

void UState::InitializeState()
{
	if (DisplayLogs)
		UE_LOG(LogTemp, Warning, TEXT("%s init state"), *this->GetName());

}


void UState::BeginState()
{
	if (DisplayLogs)
		UE_LOG(LogTemp, Warning, TEXT("%s begin state"), *this->GetName());

	OnStateBegan.Broadcast();
}

TSubclassOf<UState> UState::UpdateStateSubClass()
{
	return nullptr;
}

void UState::EndState()
{
	if (DisplayLogs)
		UE_LOG(LogTemp, Warning, TEXT("%s end state"), *this->GetName())

	OnStateEnded.Broadcast();
}

