// Fill out your copyright notice in the Description page of Project Settings.

#include "StateMachine.h"
#include "State.h"
#include "Components/ActorComponent.h"
#include "GameFramework/Actor.h"
#include "State.h"



// Sets default values for this component's properties
UStateMachine::UStateMachine()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;


}


// Called when the game starts
void UStateMachine::BeginPlay()
{
	Super::BeginPlay();

	//Get all states from components
	GetOwner()->GetComponents<UState>(AllStates);

	InitializeAllStates();

	//Set current state for the first time based on entry state
	if (EntryState)
	{
		CurrentState = EntryState;
	}
	else
	{
		if (AllStates.Num() > 0)
		{
			CurrentState = AllStates[0];
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("State machine %s doesn't get state component on %s, please add new class derivered from state class and add it as component of your actor."),*this->GetName(), *this->GetOwner()->GetName());
		}
	}

	LastState = nullptr;
	CurrentState->BeginState();
}


// Called every frame
void UStateMachine::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	TSubclassOf<UState> StateTypeReturned = CurrentState->UpdateStateSubClass();

	if (StateTypeReturned == nullptr)
	{
		return;
	}
	if(LogsDisplayed)
		UE_LOG(LogTemp, Warning, TEXT("New state type returned =  %s"), *StateTypeReturned->GetName());

	UState* ChangedState = GetStateComponentFromStateClassType(StateTypeReturned);

	//UState* ChangedState = CurrentState->UpdateState();
	
	if (ChangedState != nullptr)
	{
		CurrentState->EndState();
		LastState = CurrentState;
		OnLastStateEnded.Broadcast(LastState);
		CurrentState = ChangedState;
		
		CurrentState->BeginState();

		OnNewStateBeganTwoParam.Broadcast(CurrentState, LastState);
		OnNewStateBegan.Broadcast(CurrentState);
	}

}

void UStateMachine::InitializeAllStates()
{
	SetLogDisplayed(LogsDisplayed);

	for (UState* State: AllStates)
	{
		State->InitializeState();
	}
}

void UStateMachine::SetEntryState(UState* NewCurrentState)
{
	GetOwner()->GetComponents<UState>(AllStates);

	for (UState* State : AllStates)
	{
		if (State == NewCurrentState)
		{
			EntryState = NewCurrentState;

			return;
		}
	}

	UE_LOG(LogTemp, Error, TEXT("State referenced in your function doesn't exist on that actor, be sure that you have an instance of %s on %s"), *NewCurrentState->GetName(), *this->GetOwner()->GetName());
}

#if WITH_EDITOR
void UStateMachine::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);
	
	//Set class entry when the use change a property
	///TODO: Add a check on the property that has been changed.

	if (ClassEntryState == nullptr)
	{
		EntryState = nullptr;
	}
	else
	{
		SetEntryState(GetStateComponentFromStateClassType(ClassEntryState));	
	}
}
#endif

void UStateMachine::SetLogDisplayed(bool DisplayLogs)
{

	LogsDisplayed = DisplayLogs;

	for (UState* State : AllStates)
	{
		State->DisplayLogs = this->LogsDisplayed;
	}

}

bool UStateMachine::GetIsLogDisplayed()
{
	return LogsDisplayed;
}

/**
 *  Get a state component from class type on the current actor
 @the type of state that will be returned
 */
UState* UStateMachine::GetStateComponentFromStateClassType(TSubclassOf<UState> StateType)
{

	TArray<UActorComponent*> AllStatesOfTypeEntryState = GetOwner()->GetComponentsByClass(StateType);

	if (AllStatesOfTypeEntryState.Num() < 1)
	{
		///TODO: add a compilation error instead of a ue log to avoid any error from the game designer.

		UE_LOG(LogTemp, Error, TEXT("Warning, you have %s components of type %s on your Actor. The entry class has been setup as the first one in the hierarchy"), *FString::FromInt(AllStatesOfTypeEntryState.Num()), *ClassEntryState->GetName());
		return nullptr;
	}
	else
	{
		return Cast<UState>(AllStatesOfTypeEntryState[0]);
	}

}