// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FINITESTATEMACHINE_StateMachineActor_generated_h
#error "StateMachineActor.generated.h already included, missing '#pragma once' in StateMachineActor.h"
#endif
#define FINITESTATEMACHINE_StateMachineActor_generated_h

#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachineActor_h_12_RPC_WRAPPERS
#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachineActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachineActor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAStateMachineActor(); \
	friend FINITESTATEMACHINE_API class UClass* Z_Construct_UClass_AStateMachineActor(); \
public: \
	DECLARE_CLASS(AStateMachineActor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/FiniteStateMachine"), NO_API) \
	DECLARE_SERIALIZER(AStateMachineActor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachineActor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAStateMachineActor(); \
	friend FINITESTATEMACHINE_API class UClass* Z_Construct_UClass_AStateMachineActor(); \
public: \
	DECLARE_CLASS(AStateMachineActor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/FiniteStateMachine"), NO_API) \
	DECLARE_SERIALIZER(AStateMachineActor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachineActor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AStateMachineActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AStateMachineActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStateMachineActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStateMachineActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStateMachineActor(AStateMachineActor&&); \
	NO_API AStateMachineActor(const AStateMachineActor&); \
public:


#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachineActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStateMachineActor(AStateMachineActor&&); \
	NO_API AStateMachineActor(const AStateMachineActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStateMachineActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStateMachineActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AStateMachineActor)


#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachineActor_h_12_PRIVATE_PROPERTY_OFFSET
#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachineActor_h_9_PROLOG
#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachineActor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachineActor_h_12_PRIVATE_PROPERTY_OFFSET \
	SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachineActor_h_12_RPC_WRAPPERS \
	SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachineActor_h_12_INCLASS \
	SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachineActor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachineActor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachineActor_h_12_PRIVATE_PROPERTY_OFFSET \
	SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachineActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachineActor_h_12_INCLASS_NO_PURE_DECLS \
	SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachineActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachineActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
