// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Public/State.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeState() {}
// Cross Module References
	FINITESTATEMACHINE_API UFunction* Z_Construct_UDelegateFunction_FiniteStateMachine_InternalStateSignature__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_FiniteStateMachine();
	FINITESTATEMACHINE_API UClass* Z_Construct_UClass_UState_NoRegister();
	FINITESTATEMACHINE_API UClass* Z_Construct_UClass_UState();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	FINITESTATEMACHINE_API UFunction* Z_Construct_UFunction_UState_BeginState();
	FINITESTATEMACHINE_API UFunction* Z_Construct_UFunction_UState_EndState();
	FINITESTATEMACHINE_API UFunction* Z_Construct_UFunction_UState_InitializeState();
	FINITESTATEMACHINE_API UFunction* Z_Construct_UFunction_UState_UpdateStateSubClass();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
// End Cross Module References
	UFunction* Z_Construct_UDelegateFunction_FiniteStateMachine_InternalStateSignature__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/State.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_FiniteStateMachine, "InternalStateSignature__DelegateSignature", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00130000, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	void UState::StaticRegisterNativesUState()
	{
		UClass* Class = UState::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "BeginState", (Native)&UState::execBeginState },
			{ "EndState", (Native)&UState::execEndState },
			{ "InitializeState", (Native)&UState::execInitializeState },
			{ "UpdateStateSubClass", (Native)&UState::execUpdateStateSubClass },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	UFunction* Z_Construct_UFunction_UState_BeginState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "StateMachine" },
				{ "ModuleRelativePath", "Public/State.h" },
				{ "ToolTip", "Overitable begin state (Managed by state machine)" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_UState, "BeginState", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UState_EndState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "StateMachine" },
				{ "ModuleRelativePath", "Public/State.h" },
				{ "ToolTip", "Overitable end state (Managed by state machine)" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_UState, "EndState", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UState_InitializeState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "StateMachine" },
				{ "ModuleRelativePath", "Public/State.h" },
				{ "ToolTip", "Overitable initialize state.\n        This phase happens during the begin play of the state machine and happens only one time." },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_UState, "InitializeState", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UState_UpdateStateSubClass()
	{
		struct State_eventUpdateStateSubClass_Parms
		{
			TSubclassOf<UState>  ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			static const UE4CodeGen_Private::FClassPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Class, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0014000000000580, 1, nullptr, STRUCT_OFFSET(State_eventUpdateStateSubClass_Parms, ReturnValue), Z_Construct_UClass_UState_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "StateMachine" },
				{ "ModuleRelativePath", "Public/State.h" },
				{ "ToolTip", "Overitable update state state (Managed by state machine)\n\n        @ Return the subclass type of the new state. Return null if no state is needed during this frame." },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_UState, "UpdateStateSubClass", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020400, sizeof(State_eventUpdateStateSubClass_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UState_NoRegister()
	{
		return UState::StaticClass();
	}
	UClass* Z_Construct_UClass_UState()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_UActorComponent,
				(UObject* (*)())Z_Construct_UPackage__Script_FiniteStateMachine,
			};
			static const FClassFunctionLinkInfo FuncInfo[] = {
				{ &Z_Construct_UFunction_UState_BeginState, "BeginState" }, // 2691651788
				{ &Z_Construct_UFunction_UState_EndState, "EndState" }, // 1639582948
				{ &Z_Construct_UFunction_UState_InitializeState, "InitializeState" }, // 4146180367
				{ &Z_Construct_UFunction_UState_UpdateStateSubClass, "UpdateStateSubClass" }, // 539382352
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "BlueprintSpawnableComponent", "" },
				{ "ChildCannotTick", "" },
				{ "ClassGroupNames", "Custom" },
				{ "IncludePath", "State.h" },
				{ "ModuleRelativePath", "Public/State.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayLogs_MetaData[] = {
				{ "Category", "StateMachine" },
				{ "ModuleRelativePath", "Public/State.h" },
			};
#endif
			auto NewProp_DisplayLogs_SetBit = [](void* Obj){ ((UState*)Obj)->DisplayLogs = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_DisplayLogs = { UE4CodeGen_Private::EPropertyClass::Bool, "DisplayLogs", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(UState), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_DisplayLogs_SetBit)>::SetBit, METADATA_PARAMS(NewProp_DisplayLogs_MetaData, ARRAY_COUNT(NewProp_DisplayLogs_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_DisplayLogs,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<UState>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&UState::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00B00080u,
				FuncInfo, ARRAY_COUNT(FuncInfo),
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UState, 4212202660);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UState(Z_Construct_UClass_UState, &UState::StaticClass, TEXT("/Script/FiniteStateMachine"), TEXT("UState"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UState);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
