// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Public/StateMachineActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeStateMachineActor() {}
// Cross Module References
	FINITESTATEMACHINE_API UClass* Z_Construct_UClass_AStateMachineActor_NoRegister();
	FINITESTATEMACHINE_API UClass* Z_Construct_UClass_AStateMachineActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_FiniteStateMachine();
// End Cross Module References
	void AStateMachineActor::StaticRegisterNativesAStateMachineActor()
	{
	}
	UClass* Z_Construct_UClass_AStateMachineActor_NoRegister()
	{
		return AStateMachineActor::StaticClass();
	}
	UClass* Z_Construct_UClass_AStateMachineActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_FiniteStateMachine,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "StateMachineActor.h" },
				{ "ModuleRelativePath", "Public/StateMachineActor.h" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<AStateMachineActor>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&AStateMachineActor::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				nullptr, 0,
				nullptr, 0,
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AStateMachineActor, 1361643900);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AStateMachineActor(Z_Construct_UClass_AStateMachineActor, &AStateMachineActor::StaticClass, TEXT("/Script/FiniteStateMachine"), TEXT("AStateMachineActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AStateMachineActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
