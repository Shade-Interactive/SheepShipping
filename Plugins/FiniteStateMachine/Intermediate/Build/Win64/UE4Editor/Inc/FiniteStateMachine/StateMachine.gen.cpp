// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Public/StateMachine.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeStateMachine() {}
// Cross Module References
	FINITESTATEMACHINE_API UFunction* Z_Construct_UDelegateFunction_FiniteStateMachine_StateMachineSignatureTwo__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_FiniteStateMachine();
	FINITESTATEMACHINE_API UClass* Z_Construct_UClass_UState_NoRegister();
	FINITESTATEMACHINE_API UFunction* Z_Construct_UDelegateFunction_FiniteStateMachine_StateMachineSignature__DelegateSignature();
	FINITESTATEMACHINE_API UClass* Z_Construct_UClass_UStateMachine_NoRegister();
	FINITESTATEMACHINE_API UClass* Z_Construct_UClass_UStateMachine();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	FINITESTATEMACHINE_API UFunction* Z_Construct_UFunction_UStateMachine_GetIsLogDisplayed();
	FINITESTATEMACHINE_API UFunction* Z_Construct_UFunction_UStateMachine_SetLogDisplayed();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
// End Cross Module References
	UFunction* Z_Construct_UDelegateFunction_FiniteStateMachine_StateMachineSignatureTwo__DelegateSignature()
	{
		struct _Script_FiniteStateMachine_eventStateMachineSignatureTwo_Parms
		{
			UState* NewState;
			UState* LastState;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastState_MetaData[] = {
				{ "EditInline", "true" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LastState = { UE4CodeGen_Private::EPropertyClass::Object, "LastState", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000080080, 1, nullptr, STRUCT_OFFSET(_Script_FiniteStateMachine_eventStateMachineSignatureTwo_Parms, LastState), Z_Construct_UClass_UState_NoRegister, METADATA_PARAMS(NewProp_LastState_MetaData, ARRAY_COUNT(NewProp_LastState_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewState_MetaData[] = {
				{ "EditInline", "true" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NewState = { UE4CodeGen_Private::EPropertyClass::Object, "NewState", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000080080, 1, nullptr, STRUCT_OFFSET(_Script_FiniteStateMachine_eventStateMachineSignatureTwo_Parms, NewState), Z_Construct_UClass_UState_NoRegister, METADATA_PARAMS(NewProp_NewState_MetaData, ARRAY_COUNT(NewProp_NewState_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_LastState,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_NewState,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/StateMachine.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_FiniteStateMachine, "StateMachineSignatureTwo__DelegateSignature", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00130000, sizeof(_Script_FiniteStateMachine_eventStateMachineSignatureTwo_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UDelegateFunction_FiniteStateMachine_StateMachineSignature__DelegateSignature()
	{
		struct _Script_FiniteStateMachine_eventStateMachineSignature_Parms
		{
			UState* ReturnedState;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnedState_MetaData[] = {
				{ "EditInline", "true" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnedState = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnedState", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000080080, 1, nullptr, STRUCT_OFFSET(_Script_FiniteStateMachine_eventStateMachineSignature_Parms, ReturnedState), Z_Construct_UClass_UState_NoRegister, METADATA_PARAMS(NewProp_ReturnedState_MetaData, ARRAY_COUNT(NewProp_ReturnedState_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnedState,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/StateMachine.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_FiniteStateMachine, "StateMachineSignature__DelegateSignature", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00130000, sizeof(_Script_FiniteStateMachine_eventStateMachineSignature_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	void UStateMachine::StaticRegisterNativesUStateMachine()
	{
		UClass* Class = UStateMachine::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetIsLogDisplayed", (Native)&UStateMachine::execGetIsLogDisplayed },
			{ "SetLogDisplayed", (Native)&UStateMachine::execSetLogDisplayed },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	UFunction* Z_Construct_UFunction_UStateMachine_GetIsLogDisplayed()
	{
		struct StateMachine_eventGetIsLogDisplayed_Parms
		{
			bool ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_ReturnValue_SetBit = [](void* Obj){ ((StateMachine_eventGetIsLogDisplayed_Parms*)Obj)->ReturnValue = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(StateMachine_eventGetIsLogDisplayed_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_ReturnValue_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "StateMachine" },
				{ "ModuleRelativePath", "Public/StateMachine.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_UStateMachine, "GetIsLogDisplayed", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14020401, sizeof(StateMachine_eventGetIsLogDisplayed_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UStateMachine_SetLogDisplayed()
	{
		struct StateMachine_eventSetLogDisplayed_Parms
		{
			bool DisplayLogs;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_DisplayLogs_SetBit = [](void* Obj){ ((StateMachine_eventSetLogDisplayed_Parms*)Obj)->DisplayLogs = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_DisplayLogs = { UE4CodeGen_Private::EPropertyClass::Bool, "DisplayLogs", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(StateMachine_eventSetLogDisplayed_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_DisplayLogs_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_DisplayLogs,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "StateMachine" },
				{ "ModuleRelativePath", "Public/StateMachine.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_UStateMachine, "SetLogDisplayed", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(StateMachine_eventSetLogDisplayed_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UStateMachine_NoRegister()
	{
		return UStateMachine::StaticClass();
	}
	UClass* Z_Construct_UClass_UStateMachine()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_UActorComponent,
				(UObject* (*)())Z_Construct_UPackage__Script_FiniteStateMachine,
			};
			static const FClassFunctionLinkInfo FuncInfo[] = {
				{ &Z_Construct_UFunction_UStateMachine_GetIsLogDisplayed, "GetIsLogDisplayed" }, // 115691277
				{ &Z_Construct_UFunction_UStateMachine_SetLogDisplayed, "SetLogDisplayed" }, // 947654364
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "BlueprintSpawnableComponent", "" },
				{ "ClassGroupNames", "Custom" },
				{ "IncludePath", "StateMachine.h" },
				{ "ModuleRelativePath", "Public/StateMachine.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastState_MetaData[] = {
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "Public/StateMachine.h" },
				{ "ToolTip", "Last state of state machine" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LastState = { UE4CodeGen_Private::EPropertyClass::Object, "LastState", RF_Public|RF_Transient|RF_MarkAsNative, 0x0040000000080008, 1, nullptr, STRUCT_OFFSET(UStateMachine, LastState), Z_Construct_UClass_UState_NoRegister, METADATA_PARAMS(NewProp_LastState_MetaData, ARRAY_COUNT(NewProp_LastState_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentState_MetaData[] = {
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "Public/StateMachine.h" },
				{ "ToolTip", "Current state of the state machine." },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CurrentState = { UE4CodeGen_Private::EPropertyClass::Object, "CurrentState", RF_Public|RF_Transient|RF_MarkAsNative, 0x0040000000080008, 1, nullptr, STRUCT_OFFSET(UStateMachine, CurrentState), Z_Construct_UClass_UState_NoRegister, METADATA_PARAMS(NewProp_CurrentState_MetaData, ARRAY_COUNT(NewProp_CurrentState_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EntryState_MetaData[] = {
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "Public/StateMachine.h" },
				{ "ToolTip", "Entry state based on the class entry state" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EntryState = { UE4CodeGen_Private::EPropertyClass::Object, "EntryState", RF_Public|RF_Transient|RF_MarkAsNative, 0x0040000000080008, 1, nullptr, STRUCT_OFFSET(UStateMachine, EntryState), Z_Construct_UClass_UState_NoRegister, METADATA_PARAMS(NewProp_EntryState_MetaData, ARRAY_COUNT(NewProp_EntryState_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AllStates_MetaData[] = {
				{ "Category", "StateMachine" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "Public/StateMachine.h" },
				{ "ToolTip", "All states of the state machine (Assigned during the begin play)" },
			};
#endif
			static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AllStates = { UE4CodeGen_Private::EPropertyClass::Array, "AllStates", RF_Public|RF_Transient|RF_MarkAsNative, 0x0020088000020009, 1, nullptr, STRUCT_OFFSET(UStateMachine, AllStates), METADATA_PARAMS(NewProp_AllStates_MetaData, ARRAY_COUNT(NewProp_AllStates_MetaData)) };
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AllStates_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "AllStates", RF_Public|RF_Transient|RF_MarkAsNative, 0x00000000000a0008, 1, nullptr, 0, Z_Construct_UClass_UState_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClassEntryState_MetaData[] = {
				{ "Category", "StateMachine" },
				{ "ModuleRelativePath", "Public/StateMachine.h" },
				{ "ToolTip", "Entry state type (defined by the user in editor)" },
			};
#endif
			static const UE4CodeGen_Private::FClassPropertyParams NewProp_ClassEntryState = { UE4CodeGen_Private::EPropertyClass::Class, "ClassEntryState", RF_Public|RF_Transient|RF_MarkAsNative, 0x0024080000010001, 1, nullptr, STRUCT_OFFSET(UStateMachine, ClassEntryState), Z_Construct_UClass_UState_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(NewProp_ClassEntryState_MetaData, ARRAY_COUNT(NewProp_ClassEntryState_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnNewStateBeganTwoParam_MetaData[] = {
				{ "ModuleRelativePath", "Public/StateMachine.h" },
			};
#endif
			static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnNewStateBeganTwoParam = { UE4CodeGen_Private::EPropertyClass::MulticastDelegate, "OnNewStateBeganTwoParam", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000010080000, 1, nullptr, STRUCT_OFFSET(UStateMachine, OnNewStateBeganTwoParam), Z_Construct_UDelegateFunction_FiniteStateMachine_StateMachineSignatureTwo__DelegateSignature, METADATA_PARAMS(NewProp_OnNewStateBeganTwoParam_MetaData, ARRAY_COUNT(NewProp_OnNewStateBeganTwoParam_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnNewStateBegan_MetaData[] = {
				{ "ModuleRelativePath", "Public/StateMachine.h" },
			};
#endif
			static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnNewStateBegan = { UE4CodeGen_Private::EPropertyClass::MulticastDelegate, "OnNewStateBegan", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000010080000, 1, nullptr, STRUCT_OFFSET(UStateMachine, OnNewStateBegan), Z_Construct_UDelegateFunction_FiniteStateMachine_StateMachineSignature__DelegateSignature, METADATA_PARAMS(NewProp_OnNewStateBegan_MetaData, ARRAY_COUNT(NewProp_OnNewStateBegan_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnLastStateEnded_MetaData[] = {
				{ "ModuleRelativePath", "Public/StateMachine.h" },
			};
#endif
			static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnLastStateEnded = { UE4CodeGen_Private::EPropertyClass::MulticastDelegate, "OnLastStateEnded", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000010080000, 1, nullptr, STRUCT_OFFSET(UStateMachine, OnLastStateEnded), Z_Construct_UDelegateFunction_FiniteStateMachine_StateMachineSignature__DelegateSignature, METADATA_PARAMS(NewProp_OnLastStateEnded_MetaData, ARRAY_COUNT(NewProp_OnLastStateEnded_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_LastState,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_CurrentState,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_EntryState,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_AllStates,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_AllStates_Inner,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ClassEntryState,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_OnNewStateBeganTwoParam,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_OnNewStateBegan,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_OnLastStateEnded,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<UStateMachine>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&UStateMachine::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00B00080u,
				FuncInfo, ARRAY_COUNT(FuncInfo),
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UStateMachine, 505937724);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UStateMachine(Z_Construct_UClass_UStateMachine, &UStateMachine::StaticClass, TEXT("/Script/FiniteStateMachine"), TEXT("UStateMachine"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UStateMachine);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
