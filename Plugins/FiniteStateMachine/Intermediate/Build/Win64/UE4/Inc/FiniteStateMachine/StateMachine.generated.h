// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UState;
#ifdef FINITESTATEMACHINE_StateMachine_generated_h
#error "StateMachine.generated.h already included, missing '#pragma once' in StateMachine.h"
#endif
#define FINITESTATEMACHINE_StateMachine_generated_h

#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachine_h_10_DELEGATE \
struct _Script_FiniteStateMachine_eventStateMachineSignatureTwo_Parms \
{ \
	UState* NewState; \
	UState* LastState; \
}; \
static inline void FStateMachineSignatureTwo_DelegateWrapper(const FMulticastScriptDelegate& StateMachineSignatureTwo, UState* NewState, UState* LastState) \
{ \
	_Script_FiniteStateMachine_eventStateMachineSignatureTwo_Parms Parms; \
	Parms.NewState=NewState; \
	Parms.LastState=LastState; \
	StateMachineSignatureTwo.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachine_h_9_DELEGATE \
struct _Script_FiniteStateMachine_eventStateMachineSignature_Parms \
{ \
	UState* ReturnedState; \
}; \
static inline void FStateMachineSignature_DelegateWrapper(const FMulticastScriptDelegate& StateMachineSignature, UState* ReturnedState) \
{ \
	_Script_FiniteStateMachine_eventStateMachineSignature_Parms Parms; \
	Parms.ReturnedState=ReturnedState; \
	StateMachineSignature.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachine_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetIsLogDisplayed) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->GetIsLogDisplayed(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetLogDisplayed) \
	{ \
		P_GET_UBOOL(Z_Param_DisplayLogs); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SetLogDisplayed(Z_Param_DisplayLogs); \
		P_NATIVE_END; \
	}


#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachine_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetIsLogDisplayed) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->GetIsLogDisplayed(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetLogDisplayed) \
	{ \
		P_GET_UBOOL(Z_Param_DisplayLogs); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SetLogDisplayed(Z_Param_DisplayLogs); \
		P_NATIVE_END; \
	}


#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachine_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUStateMachine(); \
	friend FINITESTATEMACHINE_API class UClass* Z_Construct_UClass_UStateMachine(); \
public: \
	DECLARE_CLASS(UStateMachine, UActorComponent, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/FiniteStateMachine"), NO_API) \
	DECLARE_SERIALIZER(UStateMachine) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachine_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUStateMachine(); \
	friend FINITESTATEMACHINE_API class UClass* Z_Construct_UClass_UStateMachine(); \
public: \
	DECLARE_CLASS(UStateMachine, UActorComponent, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/FiniteStateMachine"), NO_API) \
	DECLARE_SERIALIZER(UStateMachine) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachine_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UStateMachine(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UStateMachine) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UStateMachine); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UStateMachine); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UStateMachine(UStateMachine&&); \
	NO_API UStateMachine(const UStateMachine&); \
public:


#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachine_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UStateMachine(UStateMachine&&); \
	NO_API UStateMachine(const UStateMachine&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UStateMachine); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UStateMachine); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UStateMachine)


#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachine_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ClassEntryState() { return STRUCT_OFFSET(UStateMachine, ClassEntryState); } \
	FORCEINLINE static uint32 __PPO__AllStates() { return STRUCT_OFFSET(UStateMachine, AllStates); } \
	FORCEINLINE static uint32 __PPO__EntryState() { return STRUCT_OFFSET(UStateMachine, EntryState); } \
	FORCEINLINE static uint32 __PPO__CurrentState() { return STRUCT_OFFSET(UStateMachine, CurrentState); } \
	FORCEINLINE static uint32 __PPO__LastState() { return STRUCT_OFFSET(UStateMachine, LastState); }


#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachine_h_14_PROLOG
#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachine_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachine_h_17_PRIVATE_PROPERTY_OFFSET \
	SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachine_h_17_RPC_WRAPPERS \
	SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachine_h_17_INCLASS \
	SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachine_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachine_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachine_h_17_PRIVATE_PROPERTY_OFFSET \
	SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachine_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachine_h_17_INCLASS_NO_PURE_DECLS \
	SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachine_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_StateMachine_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
