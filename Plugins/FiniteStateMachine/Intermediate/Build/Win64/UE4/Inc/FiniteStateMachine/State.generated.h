// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UState;
#ifdef FINITESTATEMACHINE_State_generated_h
#error "State.generated.h already included, missing '#pragma once' in State.h"
#endif
#define FINITESTATEMACHINE_State_generated_h

#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_State_h_9_DELEGATE \
static inline void FInternalStateSignature_DelegateWrapper(const FMulticastScriptDelegate& InternalStateSignature) \
{ \
	InternalStateSignature.ProcessMulticastDelegate<UObject>(NULL); \
}


#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_State_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execEndState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->EndState(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execBeginState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->BeginState(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUpdateStateSubClass) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(TSubclassOf<UState> *)Z_Param__Result=this->UpdateStateSubClass(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execInitializeState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->InitializeState(); \
		P_NATIVE_END; \
	}


#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_State_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execEndState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->EndState(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execBeginState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->BeginState(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUpdateStateSubClass) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(TSubclassOf<UState> *)Z_Param__Result=this->UpdateStateSubClass(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execInitializeState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->InitializeState(); \
		P_NATIVE_END; \
	}


#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_State_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUState(); \
	friend FINITESTATEMACHINE_API class UClass* Z_Construct_UClass_UState(); \
public: \
	DECLARE_CLASS(UState, UActorComponent, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/FiniteStateMachine"), NO_API) \
	DECLARE_SERIALIZER(UState) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_State_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUState(); \
	friend FINITESTATEMACHINE_API class UClass* Z_Construct_UClass_UState(); \
public: \
	DECLARE_CLASS(UState, UActorComponent, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/FiniteStateMachine"), NO_API) \
	DECLARE_SERIALIZER(UState) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_State_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UState(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UState) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UState); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UState); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UState(UState&&); \
	NO_API UState(const UState&); \
public:


#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_State_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UState(UState&&); \
	NO_API UState(const UState&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UState); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UState); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UState)


#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_State_h_14_PRIVATE_PROPERTY_OFFSET
#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_State_h_11_PROLOG
#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_State_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_State_h_14_PRIVATE_PROPERTY_OFFSET \
	SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_State_h_14_RPC_WRAPPERS \
	SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_State_h_14_INCLASS \
	SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_State_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_State_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_State_h_14_PRIVATE_PROPERTY_OFFSET \
	SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_State_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_State_h_14_INCLASS_NO_PURE_DECLS \
	SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_State_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SheepShipping_Plugins_FiniteStateMachine_Source_FiniteStateMachine_Public_State_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
