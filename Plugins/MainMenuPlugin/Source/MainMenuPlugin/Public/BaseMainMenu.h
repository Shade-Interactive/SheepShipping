// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "BaseMainMenu.generated.h"

class UImage;

/**
 * 
 */
UCLASS()
class MAINMENUPLUGIN_API UBaseMainMenu : public UUserWidget
{
	GENERATED_BODY()
	
	
protected:

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "MainMenu")
		float nbOfButtons;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "MainMenu")
		bool DisplayBackground = true;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "MainMenu")
	UImage* MyCBackgroundImage = nullptr;

	virtual void NativeConstruct() override;
	
	/**
	* Simple test to add a background image in a blueprint widget.
	* 
	*/
	void CreateImageWidget();

public:
#if WITH_EDITOR
	virtual void OnDesignerChanged(const FDesignerChangedEventArgs& EventArgs) override;

	virtual bool Initialize() override;
#endif


};
