// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseMainMenu.h"
#include "Blueprint/WidgetTree.h"
#include "Components/Image.h"
#include "Components/CanvasPanelSlot.h"

void UBaseMainMenu::NativeConstruct()
{
	Super::NativeConstruct();

	

}
#if WITH_EDITOR
void UBaseMainMenu::OnDesignerChanged(const FDesignerChangedEventArgs& EventArgs)
{
	Super::OnDesignerChanged(EventArgs);

	UE_LOG(LogTemp, Warning, TEXT("On Designer Changed"));


}

bool UBaseMainMenu::Initialize()
{


	bool test = Super::Initialize();

	
	//CreateImageWidget();

	return test;

}
#endif

void UBaseMainMenu::CreateImageWidget()
{
	if (MyCBackgroundImage == nullptr)
	{

		UPanelWidget* RootWidget = Cast<UPanelWidget>(GetRootWidget());
		UE_LOG(LogTemp, Warning, TEXT("root widget name = %s"), *RootWidget->GetName());


		MyCBackgroundImage = WidgetTree->ConstructWidget<UImage>(UImage::StaticClass(), TEXT("C++BackgroundImage"));
		RootWidget->AddChild(MyCBackgroundImage);

		UCanvasPanelSlot* BackgroundImageSlot = Cast<UCanvasPanelSlot>(MyCBackgroundImage->Slot);

		if (BackgroundImageSlot)
		{
			BackgroundImageSlot->SetAnchors(FAnchors(0.f, 0.f, 1.f, 1.f));
			BackgroundImageSlot->SetOffsets(FMargin(0.f, 0.f));
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("No background image slot"));
		}

	}
}
