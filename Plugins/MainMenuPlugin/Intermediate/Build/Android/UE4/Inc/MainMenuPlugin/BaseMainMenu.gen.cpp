// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Public/BaseMainMenu.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBaseMainMenu() {}
// Cross Module References
	MAINMENUPLUGIN_API UClass* Z_Construct_UClass_UBaseMainMenu_NoRegister();
	MAINMENUPLUGIN_API UClass* Z_Construct_UClass_UBaseMainMenu();
	UMG_API UClass* Z_Construct_UClass_UUserWidget();
	UPackage* Z_Construct_UPackage__Script_MainMenuPlugin();
	UMG_API UClass* Z_Construct_UClass_UImage_NoRegister();
// End Cross Module References
	void UBaseMainMenu::StaticRegisterNativesUBaseMainMenu()
	{
	}
	UClass* Z_Construct_UClass_UBaseMainMenu_NoRegister()
	{
		return UBaseMainMenu::StaticClass();
	}
	UClass* Z_Construct_UClass_UBaseMainMenu()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_UUserWidget,
				(UObject* (*)())Z_Construct_UPackage__Script_MainMenuPlugin,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "BaseMainMenu.h" },
				{ "ModuleRelativePath", "Public/BaseMainMenu.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MyCBackgroundImage_MetaData[] = {
				{ "Category", "MainMenu" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "Public/BaseMainMenu.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MyCBackgroundImage = { UE4CodeGen_Private::EPropertyClass::Object, "MyCBackgroundImage", RF_Public|RF_Transient|RF_MarkAsNative, 0x002008000009000d, 1, nullptr, STRUCT_OFFSET(UBaseMainMenu, MyCBackgroundImage), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(NewProp_MyCBackgroundImage_MetaData, ARRAY_COUNT(NewProp_MyCBackgroundImage_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayBackground_MetaData[] = {
				{ "Category", "MainMenu" },
				{ "ModuleRelativePath", "Public/BaseMainMenu.h" },
			};
#endif
			auto NewProp_DisplayBackground_SetBit = [](void* Obj){ ((UBaseMainMenu*)Obj)->DisplayBackground = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_DisplayBackground = { UE4CodeGen_Private::EPropertyClass::Bool, "DisplayBackground", RF_Public|RF_Transient|RF_MarkAsNative, 0x0020080000010005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(UBaseMainMenu), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_DisplayBackground_SetBit)>::SetBit, METADATA_PARAMS(NewProp_DisplayBackground_MetaData, ARRAY_COUNT(NewProp_DisplayBackground_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_nbOfButtons_MetaData[] = {
				{ "Category", "MainMenu" },
				{ "ModuleRelativePath", "Public/BaseMainMenu.h" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_nbOfButtons = { UE4CodeGen_Private::EPropertyClass::Float, "nbOfButtons", RF_Public|RF_Transient|RF_MarkAsNative, 0x0020080000010005, 1, nullptr, STRUCT_OFFSET(UBaseMainMenu, nbOfButtons), METADATA_PARAMS(NewProp_nbOfButtons_MetaData, ARRAY_COUNT(NewProp_nbOfButtons_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_MyCBackgroundImage,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_DisplayBackground,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_nbOfButtons,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<UBaseMainMenu>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&UBaseMainMenu::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00B01080u,
				nullptr, 0,
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBaseMainMenu, 1208777977);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBaseMainMenu(Z_Construct_UClass_UBaseMainMenu, &UBaseMainMenu::StaticClass, TEXT("/Script/MainMenuPlugin"), TEXT("UBaseMainMenu"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBaseMainMenu);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
