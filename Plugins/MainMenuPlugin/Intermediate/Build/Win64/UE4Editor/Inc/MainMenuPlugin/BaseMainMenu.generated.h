// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MAINMENUPLUGIN_BaseMainMenu_generated_h
#error "BaseMainMenu.generated.h already included, missing '#pragma once' in BaseMainMenu.h"
#endif
#define MAINMENUPLUGIN_BaseMainMenu_generated_h

#define SheepShipping_Plugins_MainMenuPlugin_Source_MainMenuPlugin_Public_BaseMainMenu_h_17_RPC_WRAPPERS
#define SheepShipping_Plugins_MainMenuPlugin_Source_MainMenuPlugin_Public_BaseMainMenu_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define SheepShipping_Plugins_MainMenuPlugin_Source_MainMenuPlugin_Public_BaseMainMenu_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBaseMainMenu(); \
	friend MAINMENUPLUGIN_API class UClass* Z_Construct_UClass_UBaseMainMenu(); \
public: \
	DECLARE_CLASS(UBaseMainMenu, UUserWidget, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/MainMenuPlugin"), NO_API) \
	DECLARE_SERIALIZER(UBaseMainMenu) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SheepShipping_Plugins_MainMenuPlugin_Source_MainMenuPlugin_Public_BaseMainMenu_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUBaseMainMenu(); \
	friend MAINMENUPLUGIN_API class UClass* Z_Construct_UClass_UBaseMainMenu(); \
public: \
	DECLARE_CLASS(UBaseMainMenu, UUserWidget, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/MainMenuPlugin"), NO_API) \
	DECLARE_SERIALIZER(UBaseMainMenu) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SheepShipping_Plugins_MainMenuPlugin_Source_MainMenuPlugin_Public_BaseMainMenu_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBaseMainMenu(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBaseMainMenu) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBaseMainMenu); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBaseMainMenu); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBaseMainMenu(UBaseMainMenu&&); \
	NO_API UBaseMainMenu(const UBaseMainMenu&); \
public:


#define SheepShipping_Plugins_MainMenuPlugin_Source_MainMenuPlugin_Public_BaseMainMenu_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBaseMainMenu(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBaseMainMenu(UBaseMainMenu&&); \
	NO_API UBaseMainMenu(const UBaseMainMenu&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBaseMainMenu); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBaseMainMenu); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBaseMainMenu)


#define SheepShipping_Plugins_MainMenuPlugin_Source_MainMenuPlugin_Public_BaseMainMenu_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__nbOfButtons() { return STRUCT_OFFSET(UBaseMainMenu, nbOfButtons); } \
	FORCEINLINE static uint32 __PPO__DisplayBackground() { return STRUCT_OFFSET(UBaseMainMenu, DisplayBackground); } \
	FORCEINLINE static uint32 __PPO__MyCBackgroundImage() { return STRUCT_OFFSET(UBaseMainMenu, MyCBackgroundImage); }


#define SheepShipping_Plugins_MainMenuPlugin_Source_MainMenuPlugin_Public_BaseMainMenu_h_14_PROLOG
#define SheepShipping_Plugins_MainMenuPlugin_Source_MainMenuPlugin_Public_BaseMainMenu_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SheepShipping_Plugins_MainMenuPlugin_Source_MainMenuPlugin_Public_BaseMainMenu_h_17_PRIVATE_PROPERTY_OFFSET \
	SheepShipping_Plugins_MainMenuPlugin_Source_MainMenuPlugin_Public_BaseMainMenu_h_17_RPC_WRAPPERS \
	SheepShipping_Plugins_MainMenuPlugin_Source_MainMenuPlugin_Public_BaseMainMenu_h_17_INCLASS \
	SheepShipping_Plugins_MainMenuPlugin_Source_MainMenuPlugin_Public_BaseMainMenu_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SheepShipping_Plugins_MainMenuPlugin_Source_MainMenuPlugin_Public_BaseMainMenu_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SheepShipping_Plugins_MainMenuPlugin_Source_MainMenuPlugin_Public_BaseMainMenu_h_17_PRIVATE_PROPERTY_OFFSET \
	SheepShipping_Plugins_MainMenuPlugin_Source_MainMenuPlugin_Public_BaseMainMenu_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	SheepShipping_Plugins_MainMenuPlugin_Source_MainMenuPlugin_Public_BaseMainMenu_h_17_INCLASS_NO_PURE_DECLS \
	SheepShipping_Plugins_MainMenuPlugin_Source_MainMenuPlugin_Public_BaseMainMenu_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SheepShipping_Plugins_MainMenuPlugin_Source_MainMenuPlugin_Public_BaseMainMenu_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
