# Sheep Shipping
Example of state machine capability by moving a small dog and linking with animation blueprint. 

## Warning
The example doesn't work when packing the game only in-editor is working for the moment. Change on referencing player controller is needed. 

##DONE
* Clean link with animation blueprint
* Add rotation to the pawn mesh and keep the camera behind the pawn (Ref crash bandicout)

## TODO

* ! Remove player controller reference from the states in the state machine example!
* Manage jump while running
* Add new state to bark
* Add ui example usage

# State Machine
Plugin giving the possiblity to add state machine component on Actors and use them in .

## Usage c++



## TODO
* Manage state inside the blueprint
* Make example of state machine blueprint usage
* Add transitions inside the state machine
* Create a custom interface to ease the setup of state machine


# Main Menu Plugin
Plugin to create without effort a main menu for PC. Capability to change the look and keep the structure. 

## Warning
This plugin is for the moment a test of widget replication from c++. 
There is a big problem with hierarchy panel that is not updated. 

##TODO
* Set the focus on the windows to solve focus problem when packed
* Test to replicate a menu based on a template (Blueprint as template and I copy in code from the widget tree class)
